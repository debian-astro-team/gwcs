Source: gwcs
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Miguel de Val-Borro <miguel@archlinux.net>,
           Ole Streicher <olebole@debian.org>,
           Phil Wyett <philip.wyett@kathenas.org>
Section: python
Priority: optional
Build-Depends: cython3,
               debhelper-compat (= 13),
               dh-python,
               libjs-jquery,
               libjs-underscore,
               pybuild-plugin-pyproject,
               python3-all,
               python3-asdf-astropy,
               python3-asdf-wcs-schemas,
               python3-astropy,
               python3-pytest,
               python3-pydata-sphinx-theme,
               python3-pytest-doctestplus,
               python3-scipy,
               python3-setuptools,
               python3-setuptools-scm,
               python3-sphinx,
               python3-sphinx-astropy,
               python3-sphinx-automodapi,
               python3-sphinxcontrib.jquery,
               python3-sphinx-copybutton,
               python3-tomli
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/gwcs
Vcs-Git: https://salsa.debian.org/debian-astro-team/gwcs.git
Homepage: https://gwcs.readthedocs.io
Rules-Requires-Root: no

Package: python3-gwcs
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Description: Tools for managing the WCS of astronomical data (Python 3)
 GWCS takes a general approach to WCS. It supports a data model which includes
 the entire transformation pipeline from input coordinates (detector by
 default) to world coordinates.  The goal is to provide a flexible toolkit
 which is easily extendible by adding new transforms and frames.
 .
 This package contains the Python 3 version of the package.

Package: python-gwcs-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: libjs-jquery,
         libjs-underscore,
         ${misc:Depends},
         ${sphinxdoc:Depends}
Description: Tools for managing the WCS of astronomical data (documentation)
 GWCS takes a general approach to WCS. It supports a data model which includes
 the entire transformation pipeline from input coordinates (detector by
 default) to world coordinates.  The goal is to provide a flexible toolkit
 which is easily extendible by adding new transforms and frames.
 .
 This is the common documentation package.
